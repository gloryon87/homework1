const burgerMenuButton = document.querySelector('.header__burger-menu');
const nav = document.querySelector('#header-navigation-menu');
burgerMenuButton.addEventListener('click', toggleMenu);
burgerMenuButton.addEventListener('touchend', toggleMenu);
function toggleMenu() {
    nav.classList.toggle('js-menu-show');
    burgerMenuButton.classList.toggle('header__burger-menu--toggle');
}