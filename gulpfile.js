const gulp = require('gulp');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const clean = require('gulp-clean');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass')(require('sass'));
const purgecss = require('gulp-purgecss');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload({stream: true});
const compress_images = require("compress-images"),

INPUT_path_to_your_images = "src/img/**/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}";
OUTPUT_path = "dist/img/";

function images(done) {
  compress_images(
    "src/img/**/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}",
    "dist/img/",
    { compress_force: false, statistic: true, autoupdate: true },
    false,
    { jpg: { engine: "mozjpeg", command: ["-quality", "60"] } },
    { png: { engine: "pngquant", command: ["--quality=20-50", "-o"] } },
    { svg: { engine: "svgo", command: "--multipass" } },
    {
      gif: { engine: "gifsicle", command: ["--colors", "64", "--use-col=web"] },
    },
    function (err, completed) {
      if (completed === true) {
      }
      done(err);
    }
  );
}

function cleanDist() {
    return gulp.src('dist', { read: false, allowEmpty: true })
      .pipe(clean());
  }
  
  function styles() {
    return gulp.src('src/scss/**/*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer())
      .pipe(cleanCSS())
      .pipe(gulp.dest('dist/css'));
  }
  
  function purge() {
    return gulp.src('dist/css/**/*.css')
      .pipe(purgecss({
        content: ['src/**/*.html']
      }))
      .pipe(gulp.dest('dist/css'));
  }
  
  function scripts() {
    return gulp.src('src/js/**/*.js')
      .pipe(concat('scripts.min.js'))
      .pipe(uglify())
      .pipe(gulp.dest('dist/js'));
  }
  
  
  function copyFiles() {
    return gulp.src(['src/**/*.*', '!src/scss', '!src/scss/**', '!src/js', '!src/js/**', '!src/img', '!src/img/**'])
      .pipe(gulp.dest('dist'));
  }
  
  exports.build = gulp.series(cleanDist, gulp.parallel(styles, scripts, images), purge, copyFiles);
  
  exports.dev = gulp.series(gulp.parallel(styles, scripts), copyFiles, serve);
  
  function serve() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch('./src/scss/**/*.scss', gulp.series(styles, copyFiles, (next) => {
        browserSync.reload()
        next()
    }))
    gulp.watch('./src/js/**/*.js', gulp.series(scripts, copyFiles, (next) => {
        browserSync.reload()
        next()
    }))
    gulp.watch('./*.html', (next) => {
        browserSync.reload()
        next()
    })
}